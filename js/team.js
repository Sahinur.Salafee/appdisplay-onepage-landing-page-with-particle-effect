
$(function(){
    var imageContainer = '.img_conatainer',
        imageList      = '.hoverimage',
        maxWidth       = 'parent', // parent or specific css width/  
        defImage       = 'img/team-large.png';
    var $imageContainer = $(imageContainer).eq(0);
    var $imageList      = $(imageList).eq(0);
    if (maxWidth === 'parent') { maxWidth = $imageContainer.width() + 'px'; }
    //Load images and set hover::
    $imageList.find('li').each(function(index){
        if (typeof $(this).data('image') === 'string') {
            $imageContainer.append(
                "<img id='imageToggle"+index+
                "' src='"+$(this).data('image')+
                "' style='max-width: "+maxWidth+"; display:none;' />"
            );
            $(this).data("image","imageToggle"+index);
            $(this).hover(
                function(){ loadImage($(this).data('image')); },
                function(){ loadImage('imageToggleDef'); }
            );  
        }
    });
    //Load default:
    $imageContainer.append(
                "<img id='imageToggleDef"+
                "' src='"+defImage+
                "' style='max-width: "+maxWidth+"' />"
    );
    //Toggle images:
    function loadImage(imageId) {
        $imageContainer.stop(true).fadeTo('slow',.3, function(){
            $(this).find('img').hide();
            $(this).find('img#'+imageId).show();
            $(this).fadeTo('100000',1);
        });
    }

});